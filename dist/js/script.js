$(document).ready(function () {
    var swiper = new Swiper('.client-slider', {
        slidesPerView: 2,
        spaceBetween: 0,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            767: {
                slidesPerView: 4,
            }
        }
      });
    $('.case-slider-container').each(function() {
        var swiper2 = new Swiper( this, {
            slidesPerView: 1,
            spaceBetween: 0,

            centeredSlides: true,
            pagination: {
                el:  $(this).parent().find('.swiper-pagination') ,
                clickable: true
            },
            fadeEffect: {
                crossFade: true
            },
            navigation: {
                nextEl: $(this).parent().find('.swiper-button-next') ,
                prevEl: $(this).parent().find('.swiper-button-prev')
            },
    });
    });
    var wow = new WOW(
        {
          mobile:       false
        }
      );
      wow.init();
    $(".menu_open__button").on("click" , function(){
        $(this).toggleClass("menu_open__button--active");
        $(".header-container").toggleClass('header-container--active')
    });

    $(".tabs-item").on("click" , function() {
        var id = $(this).attr("data-item");
        $(".tabs-item").removeClass("tabs-item--active");

        $(this).addClass("tabs-item--active");
        if(id === "all") {
            $(this).closest(".tabs-container").find(".case").show();
            return false
        }        
        $(this).closest(".tabs-container").find(".case").hide();
        $(this).closest(".tabs-container").find(".case[data-id="+id+"]").show();
    });
});
